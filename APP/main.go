package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"html"
	"encoding/json"
)

var (
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "goapi_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
)

func main() {
	
	router := mux.NewRouter()
	router.Use(prometheusMiddleware)
	router.Path("/metrics").Handler(promhttp.Handler())
	router.HandleFunc("/", Index).Methods("GET")
	router.HandleFunc("/meuip", meuIp).Methods("GET")
	log.Fatal(http.ListenAndServe(":5000", router))
}

func meuIp(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	m := make(map[string]string)
	m["meuip"] = r.RemoteAddr
	json.NewEncoder(w).Encode(m)
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Onde estou, %q", html.EscapeString(r.URL.Path))
}

// prometheusMiddleware implements mux.MiddlewareFunc.
func prometheusMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		route := mux.CurrentRoute(r)
		path, _ := route.GetPathTemplate()
		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
		next.ServeHTTP(w, r)
		timer.ObserveDuration()
	})
}