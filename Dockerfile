from golang:buster AS meugo

#MOMENTO 0 - Home do nosso projeto
WORKDIR /go/src/app
#MOMENTO 1 - Dependencias do meu codigo
RUN go get github.com/gorilla/mux && \
	go get github.com/prometheus/client_golang/prometheus && \
	go get github.com/prometheus/client_golang/prometheus/promauto && \
	go get github.com/prometheus/client_golang/prometheus/promhttp
#MOMENTO 2 - Preparando o terreno pra aplicacao
COPY APP/ /go/src/app


RUN CGO_ENABLED=0 GOOS=linux  go build main.go
#MOMENTO 3 - pegando resultado do meu build anterior e jogando em um lugar menor
FROM alpine
WORKDIR /root/
COPY --from=meugo /go/src/app/main .
EXPOSE 5000
ENTRYPOINT ./main